use recipe::Recipe;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Day {
    pub recipes_midi: Vec<Recipe>,
    pub recipes_soir: Vec<Recipe>,
    pub nb_of_recipes: usize
}