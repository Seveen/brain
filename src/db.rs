use std::fs::File;
use std::io::prelude::*;
use serde;
use serde_json;
use recipe::{Recipe};

pub fn read_file_from_json(filename: &str) -> String {
    match File::open(filename) {
        Err(e) => {
            println!("{} {}", e, "Creating file...");
            File::create(filename).expect("file not found and problem when creating it");
            "".to_string()
        },        
        Ok(mut f) => {
            let mut text = String::new();
            f.read_to_string(&mut text).expect("Something went wrong when reading a file!");
            text
        }
    }
}

pub fn write_db_to_json<T>(db: Vec<T>, filename: &str) where T: serde::Serialize {
    let file = File::create(filename).expect("problem creating file");
    serde_json::to_writer_pretty(file, &db).expect("write to json failed");
}

pub fn string_to_recipe_list(string: String) -> Vec<Recipe> {
    if string == "" {
        Vec::new()
    } else {
        let list: Vec<Recipe> = serde_json::from_str(&string).expect("Error when formatting recipe list.");
        list
    }
}