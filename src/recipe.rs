use std::f32;
use std::io;
use colored::*;
use db::*;
use rand;
use rand::Rng;
use time_custom::Day;
use chrono::prelude::*;
use std::fs::File;
use std::io::Write;
use std::process::Command;

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
enum Category {
    Null,
    Entree,
    Plat,
    Dessert
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
enum Season {
    Null,
    Ete,
    Hiver
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq)]
enum Unit {
    None,
    g,
    l,
    pc
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CategorizedRecipeList {
    category: Category,
    recipes: Vec<Recipe>,
    nb_of_recipes: usize
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Recipe {
    name: String,
    category: Category,
    ingredients: Vec<Ingredient>,
    instructions: String,
    season: Season
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Ingredient {
    name: String,
    qtt: f32,
    unit: Unit
}

impl PartialEq for Ingredient {
    fn eq(&self, other: &Ingredient) -> bool {
        self.name == other.name
    }
}

pub fn add_recipe(mut recipe_db: Vec<Recipe>, arguments: Vec<String>) {
    Command::new("cls").status().or_else(|_| Command::new("clear").status()).unwrap();
    let mut name = String::new();
    let mut str_category = String::new();
    let mut str_season = String::new();
    let mut category = Category::Null;
    let mut season = Season::Null;
    let mut ingredients: Vec<Ingredient> = Vec::new();
    let mut instructions = String::new();

    println!();
    println!("{}", "Adding a recipe".on_purple());

    println!("{}", "Enter recipe name and press enter");
    io::stdin().read_line(&mut name).unwrap();
    name = name.trim().to_string();

    println!("{}", "Category? Entree|Plat|Dessert ?");
    io::stdin().read_line(&mut str_category).unwrap();

    match str_category.trim() {
        "Entree" => category = Category::Entree,
        "Plat" => category = Category::Plat,
        "Dessert" => category = Category::Dessert,
        _ => category = Category::Null
    }

    println!("{}", "Season? Ete|Hiver?");
    io::stdin().read_line(&mut str_season).unwrap();

    match str_season.trim() {
        "Ete" => season = Season::Ete,
        "Hiver" => season = Season::Hiver,
        _ => season = Season::Null
    }

    loop {
        let mut ingredient_name = String::new();
        let mut ingredient_qtt = String::new();
        let mut unit = Unit::None;
        println!("{}", "Enter one ingredient at a time then press enter, OR if you're done leave blank and press enter");
        io::stdin().read_line(&mut ingredient_name).unwrap();
        ingredient_name = ingredient_name.trim().to_string();
        if ingredient_name == "" {
            break;
        }
        println!("{}", "Enter quantity then press enter. Supported units: g, l, pc, none");
        io::stdin().read_line(&mut ingredient_qtt).unwrap();
        if ingredient_qtt.trim() == "" {
            break;
        }
        if ingredient_qtt.contains("g") {
            unit = Unit::g;
            ingredient_qtt.retain(|c| c.is_numeric() );
        }
        if ingredient_qtt.contains("l") {
            unit = Unit::l;
            ingredient_qtt.retain(|c| c.is_numeric() );
        }
        if ingredient_qtt.contains("pc") {
            unit = Unit::pc;
            ingredient_qtt.retain(|c| c.is_numeric() );
        }

        let float_qtt = ingredient_qtt.trim().parse::<f32>().unwrap();
        ingredients.push(
            Ingredient {
                name: ingredient_name,
                qtt: float_qtt,
                unit
            }
        );
    }

    if ingredients.len() == 0 {
        panic!("No ingredients entered. Aborting");
    }

    println!("{}", "Enter preparation instructions and press enter");
    io::stdin().read_line(&mut instructions).unwrap();
    instructions = instructions.trim().to_string();

    recipe_db.push( Recipe {
        name,
        category,
        season,
        ingredients,
        instructions
    });

    write_db_to_json(recipe_db, "recipe.json");
    println!("{}", "Recipe added!".green());
}

pub fn remove_recipe(mut recipe_db: Vec<Recipe>, arguments: Vec<String>) {
    if recipe_db.len() == 0 {
        eprintln!("{}", "No recipe!");
        return;
    }
    if arguments.len() == 0 {
        eprintln!("{}", "ERROR: No argument given!");
        eprintln!("{} {}", "usage: brain recipe remove".yellow(), "NAME".on_red());
        return;
    }
    println!("{}", "This function doesn't check the existence of the recipe");
    let name = arguments[0].clone();

    recipe_db.retain(|ref e| e.name != name);

    write_db_to_json(recipe_db, "recipe.json");
    println!("{}", "Recipe removed!".green());
}

pub fn list_recipes(recipe_db: Vec<Recipe>) {
    Command::new("cls").status().or_else(|_| Command::new("clear").status()).unwrap();
    if recipe_db.len() == 0 {
        eprintln!("{}", "No recipe!");
        return;
    }
    let mut idx = 0;
    let mut choice = String::new();
    let mut chosen_idx = 0;
    for recipe in recipe_db.clone() {
        println!("{}: {}", idx, recipe.name);
        idx+=1;
    }

    println!("{}", "Enter recipe number and press enter");
    match io::stdin().read_line(&mut choice) {
        Err(e) => {
            eprintln!("{} {}", e, "Aborting".red());
            return;
        },
        Ok(_v) => choice = choice.trim().to_string()
    }
    if choice == "" {
        return;
    }

    match choice.parse::<usize>() {
        Err(e) => {
            eprintln!("{}. {}", e, "Aborting!".red());
            return;
        },
        Ok(v) => chosen_idx = v
    }
    if chosen_idx >= recipe_db.len() {
        eprintln!("{} {}", "Index out of bound...", "Aborting!".red());
        return;
    }
    println!("{}", recipe_db[chosen_idx].name.bold());
    match recipe_db[chosen_idx].category {
        Category::Entree => println!("{}", "Entree"),
        Category::Plat => println!("{}", "Plat"),
        Category::Dessert => println!("{}", "Dessert"),
        _ => println!("{}", "None")
    }
    match recipe_db[chosen_idx].season {
        Season::Ete => println!("{}", "Ete"),
        Season::Hiver => println!("{}", "Hiver"),
        _ => println!("{}", "Tout temps")
    }
    println!("");
    println!("{}", "Ingredients".on_blue());
    for ingredient in recipe_db[chosen_idx].ingredients.clone() {
        let mut unit_string = "";
        match ingredient.unit {
            Unit::g => unit_string = "g",
            Unit::l => unit_string = "l",
            Unit::pc => unit_string = "pc",
            _ => unit_string = "",
        }
        println!("{}{} {}", ingredient.qtt, unit_string, ingredient.name);
    }
    println!("");
    println!("{}", "Instructions".on_blue());
    println!("{}", recipe_db[chosen_idx].instructions);
}

pub fn spit_recipes(recipe_db: Vec<Recipe>, arguments: Vec<String>) {
    let sorted_recipes = sort_recipes_by_category(recipe_db);
    let mut week: Vec<Day> = Vec::new();

    for i in 0..7 {
        let mut recipes_midi = Vec::new();
        let mut recipes_soir = Vec::new();
        for list in sorted_recipes.clone() {
            if list.recipes.len() == 0 {continue;}
            let nbr = choose_rnd_unique_nbrs_in_range(list.nb_of_recipes, 14);
            recipes_midi.push(list.recipes[nbr[i] as usize].clone());
            recipes_soir.push(list.recipes[nbr[(i+7)] as usize].clone());
        }
        let nb = recipes_midi.len() + recipes_soir.len();
        let day = Day {
            recipes_midi,
            recipes_soir,
            nb_of_recipes: nb
        };
        week.push(day);
    }

    generate_groceries_list(&week);
    print_fancy_menu(&week);
    save_menu_to_txt(&week);
}

fn sort_recipes_by_category(recipe_db: Vec<Recipe>) -> Vec<CategorizedRecipeList> {
    let mut sorted_recipe_db:Vec<CategorizedRecipeList> = Vec::new();
    let mut entrees = CategorizedRecipeList {category: Category::Entree, recipes: Vec::new(), nb_of_recipes: 0};
    let mut plats = CategorizedRecipeList {category: Category::Plat, recipes: Vec::new(), nb_of_recipes: 0};
    let mut desserts = CategorizedRecipeList {category: Category::Dessert, recipes: Vec::new(), nb_of_recipes: 0};

    for recipe in recipe_db.clone() {
        match recipe.category {
            Category::Entree => entrees.recipes.push(recipe),
            Category::Plat => plats.recipes.push(recipe),
            Category::Dessert => desserts.recipes.push(recipe),
            _ => continue
        }
    }

    entrees.nb_of_recipes = entrees.recipes.len();
    plats.nb_of_recipes = plats.recipes.len();
    desserts.nb_of_recipes = desserts.recipes.len();

    sorted_recipe_db.push(entrees);
    sorted_recipe_db.push(plats);
    sorted_recipe_db.push(desserts);

    //println!("{:#?}", sorted_recipe_db);
    sorted_recipe_db
}

fn choose_rnd_unique_nbrs_in_range(range: usize, qtt: usize) -> Vec<u32> {
    let mut rng = rand::thread_rng();
    let mut numbers:Vec<u32> = Vec::new();

    while numbers.len() < qtt {
        if range == 0 {
            numbers.push(0);
        } else {
            numbers.push(rng.gen_range(0, range as u32));
        }
    }
    numbers
}

fn generate_groceries_list(week: &Vec<Day>) {
    let dt = Local::now();
    let mut filename = dt.format("%Y-%m-%d").to_string();
    filename.push_str("-liste.txt");
    let mut file = File::create(filename).expect("problem creating file");
    let mut all_ingredients: Vec<Ingredient> = Vec::new();

    for day in week {
        for recipe in &day.recipes_midi {
            for ingredient in &recipe.ingredients {
                match all_ingredients.iter().position(|i| i == ingredient) {
                    Some(val) => all_ingredients[val].qtt += ingredient.qtt,
                    None => all_ingredients.push(ingredient.clone())
                }
            }
        }
        for recipe in &day.recipes_soir {
            for ingredient in &recipe.ingredients {
                match all_ingredients.iter().position(|i| i == ingredient) {
                    Some(val) => all_ingredients[val].qtt += ingredient.qtt,
                    None => all_ingredients.push(ingredient.clone())
                }
            }
        }
    }

    for ingredient in all_ingredients {
        let mut unit_string = "";
        let mut formatted = ingredient.qtt.to_string();
        match ingredient.unit {
            Unit::g => unit_string = "g",
            Unit::l => unit_string = "l",
            Unit::pc => unit_string = "pc",
            _ => unit_string = "",
        }
        formatted.push_str(unit_string);
        formatted.push(' ');
        formatted.push_str(ingredient.name.trim());
        file.write(formatted.as_bytes()).expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
    }

    file.flush().expect("problem writing to file");
}

fn save_menu_to_txt(week: &Vec<Day>) {
    let dt = Local::now();
    let mut filename = dt.format("%Y-%m-%d").to_string();
    filename.push_str("-menu.txt");
    let mut file = File::create(filename).expect("problem creating file");

    let mut i = 1;
    for day in week {
        write!(file, "-----------------------------").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "{} {}", "Jour", i.to_string()).expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "-----------------------------").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "{}", "Midi").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "---------------").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        for plat in &day.recipes_midi {
            write!(file, "{}", match plat.category {
                Category::Entree => "Entree",
                Category::Plat => "Plat",
                Category::Dessert => "Dessert",
                _ => ""
            }).expect("problem writing to buffer");
            write!(file, "\r\n").expect("problem writing to buffer");
            write!(file, "*").expect("problem writing to buffer");
            write!(file, "{}", plat.name).expect("problem writing to buffer");
            write!(file, "\r\n").expect("problem writing to buffer");
        }
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "{}", "Soir").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        write!(file, "---------------").expect("problem writing to buffer");
        write!(file, "\r\n").expect("problem writing to buffer");
        for plat in &day.recipes_soir {
            write!(file, "{}", match plat.category {
                Category::Entree => "Entree",
                Category::Plat => "Plat",
                Category::Dessert => "Dessert",
                _ => ""
            }).expect("problem writing to buffer");
            write!(file, "\r\n").expect("problem writing to buffer");
            write!(file, "*").expect("problem writing to buffer");
            write!(file, "{}", plat.name).expect("problem writing to buffer");
            write!(file, "\r\n").expect("problem writing to buffer");
        }
        i = i+1;
        write!(file, "\r\n").expect("problem writing to buffer");
    }
    file.flush().expect("problem writing to file");
}

fn print_fancy_menu(week: &Vec<Day>) {
    let mut i = 1;
    for day in week {
        println!("");
        println!("{} {}", "Jour".purple(), i.to_string().purple());
        println!("");
        println!("{}", "Midi".red());
        for plat in &day.recipes_midi {
            println!("{}", match plat.category {
                Category::Entree => "Entree",
                Category::Plat => "Plat",
                Category::Dessert => "Dessert",
                _ => ""
            });
            println!("{}", plat.name.yellow());
        }
        println!("{}", "Soir".red());
        for plat in &day.recipes_soir {
            println!("{}", match plat.category {
                Category::Entree => "Entree",
                Category::Plat => "Plat",
                Category::Dessert => "Dessert",
                _ => ""
            });
            println!("{}", plat.name.yellow());
        }
        i = i+1;
    }
}