extern crate serde;
extern crate serde_json;
extern crate colored;
extern crate rand;
extern crate chrono;

#[macro_use]
extern crate serde_derive;

mod recipe;
mod db;
mod time_custom;

use std::env;
use colored::*;

#[derive(Debug, PartialEq)]
enum Mode {
    Null,
    Recipe,
    Chore,
}

#[derive(Debug, PartialEq)]
enum Operation {
    Null,
    Add,
    Remove,
    Spit,
    List
}

struct Command {
    mode: Mode,
    operation: Operation,
    tail: Vec<String>
}

fn main() {
    let mut args: Vec<String> = env::args().collect();
    let command = parse_arguments(&mut args);
    match command.mode {
        Mode::Recipe => {
            println!("{}", "Loading recipes list...".purple());
            let recipe_db = db::string_to_recipe_list(db::read_file_from_json("recipe.json"));
            println!("{}", "Done!".purple());
            match command.operation {
                Operation::Add => recipe::add_recipe(recipe_db, command.tail),
                Operation::Remove => recipe::remove_recipe(recipe_db,command.tail),
                Operation::List => recipe::list_recipes(recipe_db),
                Operation::Spit => recipe::spit_recipes(recipe_db, command.tail),
                _ => {
                    eprintln!("{}", "ERROR: no operator".red());
                    eprintln!("{} {} {} {}", "usage: brain".blue(), "recipe".blue(), "OPERATOR".on_red(), "");
                    eprintln!("{}", "expected operators:".blue());
                    eprintln!("{0: <5} {1: <10} {2}", "", "add", "Launches a wizard to add a recipe".blue());
                    eprintln!("{0: <5} {1: <10} {2}", "", "remove", "Removes a recipe".blue());
                    eprintln!("{0: <5} {1: <10} {2}", "", "list", "List all recipes".blue());
                    eprintln!("{0: <5} {1: <10} {2}", "", "spit", "Generates a menu and exports the menu and the groceries list to text".blue());
                    return;
                }
            }
        },
        Mode::Chore => {
            eprintln!("{}", "Pas encore implementé :(".red());
            return;
        },
        _ => {
            eprintln!("{}", "ERROR: No mode!".red());
            eprintln!("{} {} {}", "usage: brain".blue(), "MODE".on_red(), "");
            eprintln!("{}", "expected modes:".blue());
            eprintln!("{0: <5} {1: <15} {2}", "", "-r | recipe", "recipe manager".blue());
            eprintln!("{0: <5} {1: <15} {2}", "", "-c | chore", "chores manager".blue());
            return;
        }
    }
}

fn parse_arguments(args: &mut Vec<String>) -> Command {
    let mut tail = Vec::new();
    let mut op = "";
    let mut mo = "";
    let mut mode = Mode::Null;
    let mut operation = Operation::Null;
    
    if args.len() > 3 {
        tail = args.split_off(3);
    }

    if args.len() > 2 {
        op = args[2].as_str();
    }
    if args.len() > 1 {
        mo = args[1].as_str();
    }

    match mo {
        "-r" => mode = Mode::Recipe,
        "recipe" => mode = Mode::Recipe,
        "-c" => mode = Mode::Chore,
        "chore" => mode = Mode::Chore,
        _ => {
            mode = Mode::Null;
        }
    }

    match op {
        "add" => operation = Operation::Add,
        "remove" => operation = Operation::Remove,
        "spit" => operation = Operation::Spit,
        "list" => operation = Operation::List,
        _ => {
            operation = Operation::Null;
        }
    }

    let command = Command {
        mode,
        operation,
        tail
    };
    return command;
}
